import os
import re

class Converter:
    rex = re.compile('^([a-z0-9_]*):\\s*"([^"]*)(?<!\\\\)"\\s*(#.*)?',
            re.IGNORECASE)

    def convert(self, infile, outfile):
        """ If outfile is a directory, the output filename is deduced from
        input. """
        self.reset()
        fp = open(infile, 'r')
        self.parse(fp)
        fp.close()
        self.output(infile, outfile)

    def reset(self):
        self.parsed = {}

    def parse(self, fp):
        for line in fp.readlines():
            match = Converter.rex.match(line.strip())
            if match:
                self.parsed[match.group(1)] = match.group(2)

    def output(self, infile, outfile):
        if os.path.isdir(outfile):
            outfile = os.path.join(outfile, self.outfile(infile))
        fp = open(outfile, 'w')
        self.write_pre(fp)
        self.write_body(fp)
        self.write_post(fp)
        fp.close()

    def write_body(self, fp):
        for k, v in self.parsed.items():
            self.write_item(fp, k, v)

    def outfile(self, infile):
        outfile = self.parsed.get("name")
        if outfile:
            outfile = outfile + self.suffix()
        else:
            outfile = os.path.basename(infile)
            outfile = outfile.replace(".yml", self.suffix())
        return outfile

    def suffix(self):
        return ""

    def write_pre(self, fp):
        pass

    def write_post(self, fp):
        pass

    def parse_hex_rgb(self, s):
        r = self.parse_hex_couplet(s, 1)
        g = self.parse_hex_couplet(s, 3)
        b = self.parse_hex_couplet(s, 5)
        if len(s) >= 9:
            a = self.parse_hex_couplet(s, 7)
        else:
            a = 1.0
        return (r, g, b, a)

    def parse_hex_couplet(self, s, offset):
        return int(s[offset : offset + 2], 16) / 255.0

    def col(self, k):
        if k.startswith("col"):
            return k[3:]
        else:
            return None
