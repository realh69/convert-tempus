import converter


class Converter(converter.Converter):
    name_map = { "bg": "background",
        "bgalt": False,
        "fg": "foreground",
        "fgalt": False,
        "cr": "cursor",
        "cr2": "cursorfg",
    }

    def write_pre(self, fp):
        fp.write('[roxterm colour scheme]\n')
        fp.write('palette_size=16\n')

    def write_item(self, fp, k, v):
        col = self.col(k)
        if col is None:
            return
        name = Converter.name_map.get(col)
        if name is None:
            name = col
        elif name is False:
            return
        r = v[1:3] * 2
        g = v[3:5] * 2
        b = v[5:7] * 2
        fp.write("%s=#%s%s%s\n" % (name, r, g, b))
