convert-tempus
==============

convert-tempus is a small python program to generate Tempus color themes for
[iterm2](https://www.iterm2.com/) or [roxterm](https://realh.github.io/roxterm). 

You will also need to clone the upstream
[Tempus Themes repository](https://gitlab.com/protesilaos/tempus-themes) or
otherwise download its yaml files.

Usage
-----
```
convert-tempus --mode=MODE INPUT OUTPUT
```
`MODE` is either `iterm2` or `roxterm`.

`INPUT` is either a single yaml file from the tempus-themes repository, or the
`yaml` directory containing them.

`OUTPUT` is the output filename, or a directory if the input is a
directory.

The output files can be imported into iterm2's settings. For roxterm they can
be copied/moved to `~/.config/roxterm.sourceforge.net/Colours/`, or simply
created there in the first place.
