import converter


class Converter(converter.Converter):
    name_map = { "bg": "Background",
        "fg": "Foreground",
        "bgalt": "Selected Text",
        "fgalt": "Selection",
        "cr": "Cursor",
        "cr2": "Cursor Text",
    }

    def suffix(self):
        return ".itermcolors"

    def write_pre(self, fp):
        fp.write('<?xml version="1.0" encoding="UTF-8"?>\n')
        fp.write('<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" ')
        fp.write('"http://www.apple.com/DTDs/PropertyList-1.0.dtd">\n')
        fp.write('<plist version="1.0">\n')
        fp.write('<dict>\n')

    def write_post(self, fp):
        # Badge is based on bold red with alpha
        self.write_xml(fp, "Badge", self.parsed["col9"] + "7f")
        # Link is blue
        self.write_xml(fp, "Link", self.parsed["col4"])
        fp.write('</dict>\n')
        fp.write('</plist>\n')

    def write_item(self, fp, k, v):
        col = self.col(k)
        if not col is None:
            name = Converter.name_map.get(col)
            if not name:
                name = "Ansi " + col
            self.write_xml(fp, name, v)

    def write_xml(self, fp, name, hex):
        r, g, b, a = self.parse_hex_rgb(hex)
        fp.write('    <key>%s Color</key>\n' % name)
        fp.write('    <dict>\n')
        self.write_component(fp, "Alpha", a)
        self.write_component(fp, "Blue", b)
        fp.write('        <key>Color Space</key>\n')
        fp.write('        <string>sRGB</string>\n')
        self.write_component(fp, "Green", g)
        self.write_component(fp, "Red", r)
        fp.write('    </dict>\n')

    def write_component(self, fp, name, val):
        fp.write('        <key>%s Component</key>\n' % name)
        fp.write('        <real>%f</real>\n' % val)
